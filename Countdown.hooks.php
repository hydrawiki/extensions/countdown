<?php
/**
 * Curse Inc.
 * Countdown
 *
 * @author		Cameron Chunn
 * @copyright	(c) 2017 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Countdown
 * @link		https://gitlab.com/hydrawiki
 *
**/

class CountdownHooks {
	/**
	 * Add parser hood for Countdown tag
	 * @param  Parser $parser
	 * @return nothing
	 */
	static function onParserFirstCallInit( Parser $parser ) {
		global $wgOut;
		$wgOut->addModules('ext.countdown');
		$parser->setHook( 'countdown', 'CountdownHooks::renderTag' );
 	}

	/**
	 *  Handle the parsing of the countdown tag
	 * @param  $input
	 * @param  array   $args
	 * @param  Parser  $parser
	 * @param  PPFrame $frame
	 * @return string
	 */
 	static function renderTag( $input, array $args, Parser $parser, PPFrame $frame ) {
		global $wgOut;

		$time = isset($args['time'])?$args['time']:false;
		if (!$time) {
			return "<!-- no time passed to countdown from -->";
		}
		$phptime = strtotime($time);
		if (!$phptime) {
			return "<!-- couldn't parse datetime passed -->";
		}

		$passtime = date('D M d Y H:i:s O',$phptime);

		if (!isset($args['name']) || empty($args['name'])) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < 30; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
			$name = $randomString;
		} else {
			$name = $args['name'];
		}

		$finishmessage = isset($args['finishmessage'])?$args['finishmessage']:"Countdown finished, but no finish message set.";

		$parsed = str_replace(['<D>','<H>','<M>','<S>'],[
			'<span class="countdown_days"></span>',
			'<span class="countdown_hours"></span>',
			'<span class="countdown_minutes"></span>',
			'<span class="countdown_seconds"></span>'
		],$input);
		$parsed = "<span class=\"countdowntimer\" data-name=\"".$name."\" data-time=\"".$passtime."\" data-finishmessage=\"".$finishmessage."\">".$parsed."</span>";

		return $parsed;
 	}
}
