(function(window, $, mw) {
	"use strict";
	var timeinterval = [];
	var setupCountdownTimer = function(name, time, finishmessage) {
		timeinterval[name] = setInterval(function(){
			time = this.time;
			finishmessage = this.finishmessage;
			var t = getTimeRemaining(time);

			$('.countdowntimer[data-name=\"'+name+'\"] .countdown_days').html(t.days);
			$('.countdowntimer[data-name=\"'+name+'\"] .countdown_hours').html(t.hours);
			$('.countdowntimer[data-name=\"'+name+'\"] .countdown_minutes').html(t.minutes);
			$('.countdowntimer[data-name=\"'+name+'\"] .countdown_seconds').html(t.seconds);

			if(t.total<=0){
				clearInterval(timeinterval[name]);
				$('.countdowntimer[data-name=\"'+name+'\"]').html(finishmessage);
			}
		}.bind({time: time, finishmessage: finishmessage}),1000);
	}

	function getTimeRemaining(endtime){
		var t = Date.parse(endtime) - Date.parse(new Date());
		var seconds = Math.floor( (t/1000) % 60 );
		var minutes = Math.floor( (t/1000/60) % 60 );
		var hours = Math.floor( (t/(1000*60*60)) % 24 );
		var days = Math.floor( t/(1000*60*60*24) );
		return {
			'total': t,
			'days': days,
			'hours': hours,
			'minutes': minutes,
			'seconds': seconds
		};
	}

	$('.countdowntimer').each(function(){
		var name = $(this).attr('data-name');
		var time = $(this).attr('data-time').trim();
		var finishmessage = $(this).attr('data-finishmessage');
		setupCountdownTimer(name, time, finishmessage);
	});
})(window, jQuery, mw);
