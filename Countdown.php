<?php
/**
 * Curse Inc.
 * Countdown
 *
 * @author		Cameron Chunn
 * @copyright	(c) 2017 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Countdown
 * @link		https://gitlab.com/hydrawiki
 *
**/

if (function_exists('wfLoadExtension')) {
	wfLoadExtension('Countdown');
	wfWarn(
		'Deprecated PHP entry point used for Countdown extension. Please use wfLoadExtension instead, ' .
		'see https://www.mediawiki.org/wiki/Extension_registration for more details.'
	);
	return;
} else {
	die('This version of the Countdown extension requires MediaWiki 1.25+');
}
